#!/usr/bin/env python3

import libinjection
import json
from mitmproxy import http
from mitmproxy import proxy, options
from mitmproxy.tools.dump import DumpMaster

#Using libinjection for XSS detection
def detectxss(r):
    return list(libinjection.is_xss(r).values())[0]

#Add CSP to all of our pages
def addCSP(flow: http.HTTPFlow):
    flow.response.headers["Content-Security-Policy"]=r"default-src 'none'; script-src 'self' 'unsafe-inline' http://api.waf.netsec:5001; connect-src 'self' http://api.waf.netsec:5001; img-src 'self' http://*.media.tumblr.com https://*.media.tumblr.com; style-src 'self' https://unpkg.com;base-uri 'self';form-action 'self';"

#Adding CORS to all pages
def addCORS(flow: http.HTTPFlow):
    flow.response.headers["Access-Control-Allow-Origin"]=r"http://www.waf.netsec:5000"

#Checking the signup form for XSS
def checksignupform(flow: http.HTTPFlow):
    data = json.loads(flow.request.text)
    for row in data.values():
        if detectxss(row) == True:
            flow.request.text = ""

#Checking signup page for use of compromised admin token
def checksignupspage(flow: http.HTTPFlow):
    flow.request.headers["Access-Control-Allow-Origin"]=r"http://www.waf.netsec:5000"
    if flow.request.headers.get('X-Auth-Token') != None and 'this15fine' in flow.request.headers.get('X-Auth-Token'):
        print('This token has been compromised')
        flow.request.headers['X-Auth-Token'] = ""

#adding CORS to main page for functional signup form
def mainpagecors(flow: http.HTTPFlow):
    flow.request.headers["Access-Control-Request-Method"]=r"POST"
    flow.request.headers['Access-Control-Request-Headers']=r"content-type"

#Using MITM proxy to handle request and response flows.

class WAF(object):

    def request(self, flow: http.HTTPFlow):

        if flow.request.url == 'http://api.waf.netsec:5001/api/v1/signup':
            if flow.request.method == 'POST':
               checksignupform(flow)

        if flow.request.url == 'http://api.waf.netsec:5001/api/v1/signups':
            checksignupspage(flow)

        for k, v in flow.request.headers.items():
            if detectxss(v) == True:
                flow.request.headers[k]=""

        if flow.request.url == 'http://www.waf.netsec:5000/':
            mainpagecors(flow)

        if flow.request.content != b"":
            try:
                if detectxss(flow.request.content.decode("utf-8")) == True:
                    flow.request.content=b""
            except:
                print("Non-decodable content")

    def response(self, flow: http.HTTPFlow):

        #Inline script and meta tags cause false positives in libinjection, as such we remove them and evaluate based on the sanitized response to see if anything has been injected.
        if flow.response.headers.get('Content-Type') != None and 'utf-8' in flow.response.headers.get('Content-Type') or flow.response.headers.get('content-type') != None and 'utf-8' in flow.response.headers.get('content-type'):
            if 'signupForm' in flow.response.content.decode("utf-8"):
                        san = flow.response.content.decode("utf-8").replace(
                        """<script src="/static/js/pricing.js"></script>
<script>
    const signupForm = document.getElementById("signup-form");
    if (signupForm !== null) {
        signupForm.addEventListener("submit", onSignup);
    }
</script>""",'')
                        san = san.replace(
                        """<!doctype html>""",'')
                        san = san.replace(
                        """<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pricing Table</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.5/build/pure-min.css">
    <link rel="stylesheet" href="/static/css/layouts/pricing.css">
</head>""",'')
                        if detectxss(san) == True:
                            flow.response.content=b""

            elif 'getSignups();' in flow.response.content.decode("utf-8"):
                san = flow.response.content.decode("utf-8").replace(
                        """<script src="/static/js/pricing.js"></script>
<script>
    window.onload = () => {
        getSignups();
    };
</script>""",'')

                san = san.replace(
                        """<!doctype html>""",'')
                san = san.replace(
                        """<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signups</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.5/build/pure-min.css">
    <link rel="stylesheet" href="/static/css/layouts/pricing.css">
</head>""",'')

                if detectxss(san) == True:
                    flow.response.content=b""

        for k, v in flow.response.headers.items():
            if detectxss(v) == True:
                flow.response.headers[k]=""

        addCSP(flow)
        addCORS(flow)


addons = [WAF()]
