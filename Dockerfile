FROM python:3
WORKDIR /tmp
COPY /src/requirements.txt ./
RUN pip install Cython
RUN pip install --no-cache-dir -r requirements.txt
COPY /src /tmp
ENTRYPOINT ["mitmdump","-s","waf.py"]

